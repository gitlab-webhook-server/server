FROM python:3.7.0

RUN pip install --no-cache-dir waitress

COPY setup.py setup.py
COPY gitlabwebhookserver gitlabwebhookserver/

RUN pip install -e .

CMD ["waitress-serve", "--port=5000", "--call", "gitlabwebhookserver:create_app"]