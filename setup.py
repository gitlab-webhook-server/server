from setuptools import find_packages, setup

setup(
    name='gitlabwebhookserver',
    version='1.0.0',
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
        'flask~=1.1.2',
        'requests~=2.25.0',
    ],
)