import os
from flask import Flask, jsonify, request
from . import webhooks, exceptions


def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

app = Flask(__name__, instance_relative_config=True)
app.register_error_handler(exceptions.InvalidUsage, handle_invalid_usage)
app.config.from_pyfile('config.py', silent=True)
app.register_blueprint(webhooks.bp)

access_token = app.config.get("WEBHOOK_ACCESS_TOKEN")

@app.before_request
def check_access_token():
    if access_token != None and request.headers.get("X-Gitlab-Token") == access_token:
        pass
    else:
        return jsonify({"error": "access_token missing or incorrect"})

@app.route('/')
def root():
    return jsonify({})
