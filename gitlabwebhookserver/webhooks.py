from flask import (
    Blueprint, request, jsonify
)
from flask import current_app as app
import requests
from . import exceptions


bp = Blueprint("webhooks", __name__, url_prefix="/webhooks")
    

@bp.route("/", methods=["POST"])
def root():
    if not request.is_json:
        raise exceptions.InvalidUsage("Request is not valid JSON", status_code=400)

    veracode_schedule_description = "Daily Veracode Scan"
    veracode_pipeline_variable = ("VERACODE", "true")
    pipeline_schedule_cron = "0 0 * * *"
    gitlab_api_base = app.config["GITLAB_API_BASE"]
    auth_header = {"Private-Token": app.config["GITLAB_ACCESS_TOKEN"]}

    request_data = request.get_json()

    if request_data.get("event_name") == "push":
        branch = request_data["ref"][len("refs/heads/"):]
        project_id = request_data["project"]["id"]
        schedules_api = f"{gitlab_api_base}/projects/{project_id}/pipeline_schedules"
        auth_header = {"Private-Token": app.config["GITLAB_ACCESS_TOKEN"]}

        response = requests.get(schedules_api, headers=auth_header)
        if not response.ok:
            raise exceptions.InvalidUsage("Unable to get pipeline schedules", status_code=response.status_code)
        pipeline_schedules_data = response.json()

        # Check if schedule already exists for this branch
        for schedule in pipeline_schedules_data:
            if schedule["ref"] == branch and schedule["description"] == veracode_schedule_description:
                # If this is a standard commit do nothing and return
                if request_data["after"] != "0000000000000000000000000000000000000000":
                    return jsonify({"message": f"Schedule for {branch} already exists"})
                else:
                    # This is a branch delete, delete pipeline schedule
                    response = requests.delete(f"{schedules_api}/{schedule['id']}", headers=auth_header)
                    if not response.ok:
                        raise exceptions.InvalidUsage(f"Unable to delete pipeline schedule for {branch}", status_code=response.status_code)
                    return jsonify({"message": f"Schedule for {branch} deleted"})

        # No schedule found, so create one if this is a new branch
        if request_data["before"] == "0000000000000000000000000000000000000000":
            # Create new schedule
            new_pipeline_schedule = {
                "description": veracode_schedule_description,
                "ref": branch,
                "cron": pipeline_schedule_cron
            }
            response = requests.post(schedules_api, data=new_pipeline_schedule, headers=auth_header)
            if not response.ok:
                raise exceptions.InvalidUsage("Unable to create new pipeline schedule", status_code=response.status_code)
            new_pipeline_schedule_data = response.json()

            # Create new schedule variable
            pipeline_schedule_id = new_pipeline_schedule_data["id"]
            schedules_variable_api = f"{schedules_api}/{pipeline_schedule_id}/variables"
            new_pipeline_schedule_variable = {
                "key": veracode_pipeline_variable[0],
                "value": veracode_pipeline_variable[1]
            }
            response = requests.post(schedules_variable_api, data=new_pipeline_schedule_variable, headers=auth_header)
            if not response.ok:
                return jsonify({"message": f"Pipeline schedule created for {branch}, unable to create pipeline schedule variable"})

            # Run a first Veracode pipeline on this branch
            pipeline_api = f"{gitlab_api_base}/projects/{project_id}/pipeline"
            new_pipeline = {
                "ref": branch,
                "variables": [{"key": veracode_pipeline_variable[0], "value": veracode_pipeline_variable[1]}]
            }
            response = requests.post(pipeline_api, json=new_pipeline, headers=auth_header)
            if not response.ok:
                return jsonify({"message": f"Pipeline schedule created for {branch}, unable to create first Veracode pipeline"})
        
            # Everything worked a-ok
            return jsonify({"message": f"Pipeline schedule created for {branch}, first Veracode pipeline created"})
        elif request_data["after"] == "0000000000000000000000000000000000000000":
            # This is a branch delete, but there was no schedule, so nothing to do here
            return jsonify({"message": f"No schedule found for deleted branch {branch}, nothing to do"})
        else:
            # This is commit to a branch that probably existed before webhook integration was enabled, do nothing
            return jsonify({"message": f"Schedule not created for pre-existing branch {branch}"})
    else:
        raise exceptions.InvalidUsage("Event type not supported", status_code=200)
